import pandas as pd
import numpy as np
from long_seqs import long_seqs
import matplotlib.pyplot as plt
import seaborn as sns; sns.set(color_codes=True)
from itertools import combinations
from pprint import pprint

def heatmap(comparison_file, etha, ethb):

    with open(comparison_file) as f:
        ident = {}
        subjects = set()
        for l in f.readlines():
            try:
                if comparison_file == 'identities':
                    splitter = '.match # Identity: '
                elif comparison_file == 'similarities':
                    splitter = '.match # Similarity: '

                pair, identity = l.split(splitter)
                identity = identity.strip()
            except ValueError:
                continue

            subject1, subject2 = pair.split('_vs_')

            eth1, sid1 = subject1.split('_')
            eth2, sid2 = subject2.split('_')

            if (((eth1, sid1) in long_seqs and (eth2, sid2) in long_seqs)
                and (eth1 in (etha, ethb)
                     and eth2 in (etha, ethb))):

                ratio, percentage = identity.split(" ")
                numerator, denominator = ratio.split("/")

                ident[(eth1, sid1, eth2, sid2)] = int(numerator)/float(denominator)
                ident[(eth2, sid2, eth1, sid1)] = int(numerator)/float(denominator)
                subjects.add((eth1, sid1))
                subjects.add((eth2, sid2))

    subjects = sorted(list(subjects))

    data = {}
    for s1 in subjects:
        if s1[0] not in (etha, ):
            continue
        data[s1] = []
        for s2 in subjects:
            if s2[0] not in (ethb, ):
                continue
            data[s1].append(ident.get(s1 + s2, 0))


    df = pd.DataFrame.from_dict(data)
    
    f, ax = plt.subplots(figsize=(11, 11))

    sns.heatmap(df, square=True, xticklabels=False, yticklabels=False, ax=ax, cbar=False)
    # ax.set_title("%s: %s - %s" % (comparison_file, etha.capitalize(), ethb.capitalize()))
    ax.set_xlabel(etha)
    ax.set_ylabel(ethb)
    f.savefig('heatmaps/inter_%s_%s_%s_map.png' % (comparison_file, etha, ethb))

    return df


for comparison_file in ['identities', 'similarities']:
    for pair in combinations(['tlahuicas', 'mazahua', 'matlatzincas', 'otomies'], 2):
        etha, ethb = pair
        df = heatmap(comparison_file, etha, ethb)
