from itertools import combinations

with open('data/ethnicities.csv') as f:
    seqs = [seq.strip() for seq in f.readlines()]


for pair in combinations(seqs, 2):
    out = "%s_vs_%s.match" % (pair[0].replace('.seq', ''),
                              pair[1].replace('.seq', ''))
    command = "matcher ../data/%s ../data/%s %s" % (pair[0],
                                    pair[1],
                                    out)
    print(command)
