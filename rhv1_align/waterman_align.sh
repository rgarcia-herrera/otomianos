#!/bin/bash

input="seqs"
while IFS= read -r line
do
    dir=`dirname $line`
    dir=`echo $dir | sed s:/:_:g`
    dir=`echo $dir | sed s:\ :_:g`
    seq=`basename $line`
    out="${dir}_${seq}.water"

    water -asequence ../rhv1.fasta \
	  -bsequence ../$line \
	  -datafile EDNAFULL -gapopen 10.0 -gapextend 0.5 \
	  -outfile $out

    echo $out
done < "$input"
