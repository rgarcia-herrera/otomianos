grep matla identities | awk -F \: '{print $3}' | awk '{print $1}' > identities_matlatzincas.csv
grep mazahua identities | awk -F \: '{print $3}' | awk '{print $1}' > identities_mazahuas.csv
grep tlahui identities | awk -F \: '{print $3}' | awk '{print $1}' > identities_tlahuicas.csv
grep otomi identities | awk -F \: '{print $3}' | awk '{print $1}' > identities_otomies.csv


grep matla similarities | awk -F \: '{print $3}' | awk '{print $1}' > similarities_matlatzincas.csv
grep mazahua similarities | awk -F \: '{print $3}' | awk '{print $1}' > similarities_mazahuas.csv
grep tlahui similarities | awk -F \: '{print $3}' | awk '{print $1}' > similarities_tlahuicas.csv
grep otomi similarities | awk -F \: '{print $3}' | awk '{print $1}' > similarities_otomies.csv
