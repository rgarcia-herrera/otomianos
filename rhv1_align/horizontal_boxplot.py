import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

sns.set(style="ticks")

ethnicities = {'matlatzincas': [],
               'mazahuas': [],
               'otomies': [],
               'tlahuicas': []}

for like in ('similarities', 'identities'):
    # Initialize the figure with a logarithmic x axis
    fig, ax = plt.subplots(figsize=(11, 11))

    for eth in ethnicities.keys():
        with open('%s_%s.csv' % (like, eth)) as f:
            for row in f.readlines():
                numerador, denominador = [float(c) for c in row.strip().split('/')]
                ethnicities[eth].append(numerador / denominador)

    aguas = {key: pd.Series(ethnicities[key], name=key)
            for key in ethnicities.keys()}

    planets = pd.concat(aguas.values(), axis=1)

    # Plot the orbital period with horizontal boxes
    sns.boxplot(data=planets,
                whis="range", palette="vlag", ax=ax)

    # Add in points to show each observation
    sns.swarmplot(data=planets,
                  size=2, color=".3", linewidth=0, ax=ax)

    # Tweak the visual presentation
    ax.xaxis.grid(True)
    ax.set(ylabel="")
    sns.despine(trim=True, left=True)

    fig.savefig("%s_boxplot.png" % like)

