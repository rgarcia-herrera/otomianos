from itertools import combinations

#

ethnicities = ['tlahuicas', 'mazahua', 'matlatzincas', 'otomies']

keep = set()
for pair in combinations(ethnicities, 2):
    etha, ethb = pair

    keep.add((etha, ethb))


ethnicities.reverse()

for comparison_file in ['identities', 'similarities']:
    print("\n\n# %s\n\n" % comparison_file.capitalize())
    print("<table>")
    for i in ethnicities:
        print("<tr>")
        for j in ethnicities:
            if (i, j) in keep:
                img = 'inter_%s_%s_%s_map.png' % (comparison_file, i, j)
                print("<td><img src=\"%s\" width=\"100%%\"/></td>" % img)
            else:
                print('<td>%s - %s</td>' % (i, j))
        print("</tr>")
    print("</table>")
