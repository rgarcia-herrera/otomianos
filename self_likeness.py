import pandas as pd
import numpy as np
from long_seqs import long_seqs
import matplotlib.pyplot as plt
import seaborn as sns; sns.set(color_codes=True)


def heatmap(comparison_file, ethnicity):

    with open(comparison_file) as f:
        ident = {}
        subjects = set()
        for l in f.readlines():
            try:
                if comparison_file == 'identities':
                    splitter = '.match # Identity: '
                elif comparison_file == 'similarities':
                    splitter = '.match # Similarity: '

                pair, identity = l.split(splitter)
                identity = identity.strip()
            except ValueError:
                continue

            subject1, subject2 = pair.split('_vs_')

            eth1, sid1 = subject1.split('_')
            eth2, sid2 = subject2.split('_')

            if (((eth1, sid1) in long_seqs and (eth2, sid2) in long_seqs)
                and (eth1 in (ethnicity, )
                     and eth2 in (ethnicity, ))):

                ratio, percentage = identity.split(" ")
                numerator, denominator = ratio.split("/")

                ident[(eth1, sid1, eth2, sid2)] = int(numerator)/float(denominator)
                ident[(eth2, sid2, eth1, sid1)] = int(numerator)/float(denominator)
                subjects.add((eth1, sid1))
                subjects.add((eth2, sid2))

    subjects = sorted(list(subjects))

    data = {}
    for s1 in subjects:
        data[s1] = []
        for s2 in subjects:
            data[s1].append(ident.get(s1 + s2, 0))

    df = pd.DataFrame.from_dict(data)

    mask = np.zeros_like(df, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = True

    f, ax = plt.subplots(figsize=(11, 11))

    sns.heatmap(df, square=True, mask=mask, xticklabels=False, yticklabels=False, ax=ax)
    ax.set_title("%s - %s" % (ethnicity.capitalize(), comparison_file))
    ax.set_xlabel('')
    f.savefig('%s_%s_map.png' % (comparison_file, ethnicity))



for comparison_file in ['identities', 'similarities']:
    for ethnicity in ['tlahuicas', 'mazahua', 'matlatzincas', 'otomies']:
        heatmap(comparison_file, ethnicity)
