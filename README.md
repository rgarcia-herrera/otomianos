# Linajes mitocondriales de la familia lingüística otomiana

Comparaciones de secuencias de ADN mitocondrial.

## Datos

(Falta completar esta tabla contando cuántas secuencias hay de cada grupo)


|Grupo       | Secuencias
|------------|-----------
|Matlatzincas|
|Mazahua     |
|Otomies     |
|Tlahuicas   |

Secuencias se refiere a archivos formato FASTA que contienen la salida
de un secuenciador genómico. Cada archivo corresponde a una muestra de
saliva procesada. Están todos en [data](el directorio de datos).

## Procesamiento de datos

Hicimos dos experimentos:
 1. alinear cada secuencia a la [secuencia de referencia RHV1](rhv1.fasta)
 2. alinear las secuencias entre sí

El algoritmo reporta dos índices:
 1. identidad
 2. similaridad

Para las alineaciones usamos [EMBOSS](http://emboss.sourceforge.net/).

    EMBOSS: The European Molecular Biology Open Software Suite (2000)
    Rice,P. Longden,I. and Bleasby,A.
    Trends in Genetics 16, (6) pp276--277


### Alineación a RHV1

La alineación de cada secuencia a RHV1 se hizo con [rhv1_align/waterman_align.sh](este script).

La parte de interés es la siguiente, donde $line representa una secuencia cualquiera:

    water -asequence ../rhv1.fasta \
          -bsequence ../$line \
          -datafile EDNAFULL -gapopen 10.0 -gapextend 0.5 \
          -outfile $out

Este comando genera un reporte para cada alineación, que incluye una proporción para **identidad** y otra proporción para **similaridad**.

Filtrando los archivos de salida se obtiene un meta-reporte, aquí el de [similaridades](rhv1_align/similarities) acá el de [identidades](rhv1_align/identities).


### Alineaciones entre secuencias

Aquí un ejemplo de alineaciones de secuencias entre sí:

    matcher ../data/matlatzincas_100.seq ../data/tlahuicas_447.seq matlatzincas_100_vs_tlahuicas_447.match
    matcher ../data/matlatzincas_100.seq ../data/tlahuicas_448.seq matlatzincas_100_vs_tlahuicas_448.match
    matcher ../data/matlatzincas_100.seq ../data/tlahuicas_449.seq matlatzincas_100_vs_tlahuicas_449.match
    matcher ../data/matlatzincas_100.seq ../data/tlahuicas_450.seq matlatzincas_100_vs_tlahuicas_450.match

La lista total de todos los comandos a través de los cuáles se hicieron las
alineaciones está en el directorio "[matches](matches)".

De igual manera que en la sección anterior, este comando genera un reporte para cada alineación, que incluye una proporción para **identidad** y otra proporción para **similaridad**.

Filtrando los archivos de salida se obtiene un meta-reporte, aquí el de [similaridades](similarities) acá el de [identidades](identities).


## Análisis

Usando los archivos de identidades y similaridades de ambos experimentos 
(alineación a referencia y entre sí) se generaron visualizaciones.

En los mapas de calor, más "caliente" (blanco, como metal al rojo vivo) representa más similitud o identidad, mientras que más "frío" (negro, rojo obscuro) representa menos similitud o identidad.

### Identidad y similitud en alineación a referencia RHV1

Los box-plot revelan la distribución de similaridades e identidades. Cada punto es una secuencia, 
su lugar en el boxplot revela cuánto así se asemeja a la secuencia de referencia: secuencias
exactamente iguales a la referencia obtienen un valor igual a 1, secuencias totalmente diferentes
obtienen valores iguales a cero. 

#### Identidad
<img src="rhv1_align/identities_boxplot.png" />

#### Similitud
<img src="rhv1_align/similarities_boxplot.png" />


### Identidad intra-grupo

Se computaron las identidades en las alineaciones de las secuencias de
sujetos dentro del mismo grupo.

Los mapas de calor son cuadrados pues se trata de colecciones de sujetos
comparados contra los mismos sujetos dentro del mismo grupo. Mostramos sólo
la mitad inferior del cuadro por que la otra mitad es exactamente igual pues
se trata de la misma comparación puesta al revés, e.g. matlatzinca-5 vs tlahuica-3 es exactamente igual que tlahuica-3 vs matlatzinca-5.

<table><tr>
<Td>Matlatzincas</td>
<Td>Mazahua</td>
<Td>Otomies</td>
<Td>Tlahuicas</td>
</tr><tr>
<td><img src="heatmaps/identities_matlatzincas_map.png" width="100%" /></td>
<td><img src="heatmaps/identities_mazahua_map.png" width="100%" /></td>
<td><img src="heatmaps/identities_otomies_map.png" width="100%" /></td>
<td><img src="heatmaps/identities_tlahuicas_map.png" width="100%" /></td>
</tr></table>

### Identidad Inter-grupo

Se computaron las identidades combinando cada sujeto de un grupo con
cada sujeto de otro grupo. La magnitud del lado de cada rectángulo es
proporcional al tamaño del grupo en nuestro muestreo, por eso no son cuadrados
como en la sección anterior.

<table>
<tr>
<td>otomies</td>
<td>matlatzincas</td>
<td>mazahua</td>
<td> </td>
</tr>
<tr>
<td><img src="heatmaps/inter_identities_matlatzincas_otomies_map.png" width="100%"/></td>
<td></td>
<td></td>
<td>matlatzincas</td>
</tr>
<tr>
<td><img src="heatmaps/inter_identities_mazahua_otomies_map.png" width="100%"/></td>
<td><img src="heatmaps/inter_identities_mazahua_matlatzincas_map.png" width="100%"/></td>
<td></td>
<td>mazahua</td>
</tr>
<tr>
<td><img src="heatmaps/inter_identities_tlahuicas_otomies_map.png" width="100%"/></td>
<td><img src="heatmaps/inter_identities_tlahuicas_matlatzincas_map.png" width="100%"/></td>
<td><img src="heatmaps/inter_identities_tlahuicas_mazahua_map.png" width="100%"/></td>
<td>tlahuicas</td>
</tr>
</table>


### Similaridades



#### Intra-grurpo

<table><tr>
<Td>Matlatzincas</td>
<Td>Mazahua</td>
<Td>Otomies</td>
<Td>Tlahuicas</td>
</tr><tr>
<td><img src="heatmaps/similarities_matlatzincas_map.png" width="100%" /></td>
<td><img src="heatmaps/similarities_mazahua_map.png" width="100%" /></td>
<td><img src="heatmaps/similarities_otomies_map.png" width="100%" /></td>
<td><img src="heatmaps/similarities_tlahuicas_map.png" width="100%" /></td>
</tr></table>

#### Inter-grupo

<table>
<tr>
<td> otomies</td>
<td> matlatzincas</td>
<td> mazahua</td>
<td> </td>
</tr>
<tr>
<td><img src="heatmaps/inter_similarities_matlatzincas_otomies_map.png" width="100%"/></td>
<td></td>
<td></td>
<td>matlatzincas</td>
</tr>
<tr>
<td><img src="heatmaps/inter_similarities_mazahua_otomies_map.png" width="100%"/></td>
<td><img src="heatmaps/inter_similarities_mazahua_matlatzincas_map.png" width="100%"/></td>
<td></td>
<td>mazahua</td>
</tr>
<tr>
<td><img src="heatmaps/inter_similarities_tlahuicas_otomies_map.png" width="100%"/></td>
<td><img src="heatmaps/inter_similarities_tlahuicas_matlatzincas_map.png" width="100%"/></td>
<td><img src="heatmaps/inter_similarities_tlahuicas_mazahua_map.png" width="100%"/></td>
<td>tlahuicas</td>
</tr>
</table>
